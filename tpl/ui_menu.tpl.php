<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<div class="wrap">
	<h2>Universal Importer</h2>

    <div id="progressbar"><div class="progress-label">Loading...</div></div>

	<p id="step-description"></p>
</div>

 <style>
.ui-progressbar {
position: relative;
}
.progress-label {
position: absolute;
left: 50%;
top: 4px;
font-weight: bold;
text-shadow: 1px 1px 0 #fff;
}
</style>
<script>

$(function() {
    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        data: {action: 'ui_ajax_import'},
        dataType: "json",
        timeout: 3600000
    });

    var progressbar = jQuery( "#progressbar" ),
    progressLabel = jQuery( ".progress-label" );

    progressbar.progressbar({
        value: false,
        change: function() {
        	progressLabel.text( progressbar.progressbar( "value" ) + "%" );
        },
        complete: function() {
        	progressLabel.text( "Importazione completata!" );
        }
    });

    function progress() {
        jQuery.post(ajaxurl, {action: 'ui_ajax_current_step'}, function(data) {
        	progressbar.progressbar( "value", data.step);

        	jQuery("#step-description").html(data.message);

        	if ( data.step <= 100.00 ) {
            	setTimeout( progress, 5000 );
            }
    	}, 'json');
    }

    setTimeout( progress, 5000 );
});
</script>
