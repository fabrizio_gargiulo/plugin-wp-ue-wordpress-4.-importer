<?php

function fg_log($msg)
{
    date_default_timezone_set('Europe/Rome');

    $path = "/var/log/wordpress";
    $prefix = "wordpress";

    if (!is_dir($path)) {
        return ;
    }

    $file_name = sprintf("%s/%s-%s.log", $path, $prefix, date("Ymd"));
    $f = fopen($file_name, "a");

    $string = sprintf("[%s %s] %s\n", date("Y-m-d H:i:s"), 'antoniopreto.new', $msg);
    fwrite($f, $string);

    fclose($f);
}

function ui_is_importer_executing()
{
    $locked = get_option('UI_IMPORTER_IS_EXECUTING', 0);

    if ($locked == 1) {
        fg_log("Importer is locked");
    }

    return $locked;
}

function ui_lock_importer()
{
    fg_log("locking importer");
    update_option('UI_IMPORTER_IS_EXECUTING', 1);
}

function ui_unlock_importer()
{
    fg_log("unlocking importer");
    update_option('UI_IMPORTER_IS_EXECUTING', 0);
}

function ui_calculate_step($current, $total)
{
    if (($total == 0) || ($current == $total) ) {
        return 100;
    }

    $percentage = ($current * 100)/ $total;

    return round($percentage, 2);
}

function ui_ajax_set_step($current, $total, $message)
{
    update_option(UI_STEP, array(
        'current' => $current,
        'total' => $total,
        'step' => ui_calculate_step($current, $total),
        'message' => $message
    ));
}

function ui_ajax_init_steps($total)
{
    fg_log("===================================");
    update_option(UI_STEP, array(
        'current' => 0,
        'total' => $total,
        'step' => 0,
        'message' => 'Loading...'
    ));
}

function ui_ajax_set_next_step($message)
{
    $ui_step = get_option(UI_STEP);

    $ui_step['current'] += 1;
    $ui_step['step'] = ui_calculate_step($ui_step['current'], $ui_step['total']);
    $ui_step['message'] = $message;

    fg_log("UI_STEP: ".serialize($ui_step));

    update_option(UI_STEP, $ui_step);
}

function ui_seconds_to_string($seconds)
{
    $string = "";
    $hours = floor($seconds / 3600);
    $seconds = $seconds % 3600;

    $minutes = floor($seconds / 60);
    $seconds = $seconds % 60;

    if ($hours > 0) {
        $string .= $hours." hh ";
    }

    if ($hours > 0 || $minutes > 0) {
        $string .= $minutes." min ";
    }

    $string .= $seconds." sec";

    return $string;
}