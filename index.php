<?php

ini_set("display_errors", 1);
error_reporting(E_ALL);

/**
Plugin Name: Universal Importer for Wordpress
Plugin URI: N/A
Description: ...
Version: 1.0
Author: Fabrizio gargiulo
Author URI: http://www.fabriziogargiulo.com
 **/

// Include config file
require_once("config.php");

// Include functions file
require_once("functions.php");

// Include AJAX file
require_once("ajax.php");

function ui_theme($tpl_name, array $args = array())
{
    $output = '';

    try {
        ob_start();

        foreach ($args as $key => $value) {
            $$key = $value;
        }

        include "tpl/{$tpl_name}.tpl.php";

        $output = ob_get_contents();
        ob_end_clean();
    }
    catch (Exception $e) {
        die($e->getMessage());
    }

    return $output;
}

//add_action('init', 'ui_init');

function ui_init()
{
    ini_set('display_errors', 1);

    $started = time();

    require_once 'core/auto_loader.php';
    require_once 'csm/auto_loader.php';

    $importer = new CSMWordpressImporter();
    $xml_path = realpath(dirname(__FILE__)."/xml/universal-export.xml");
    $importer->import($xml_path);

    $elapsed = time() - $started;

    die("Elapsed: ".$elapsed);
}

function ui_dashboard()
{
	global $wpdb;

	if (!empty($_POST['action']) && ($_POST['action'] == 'import_xml')) {
		$error_message = '';

		/*
		try {
			$upload_dir = wp_upload_dir();

			$move_to = $upload_dir['basedir'].'/';

			if ($_FILES["file_xml"]["error"] > 0) {
				throw new PAExceptionFileNotUploaded("Return Code: " . $_FILES["file_xml"]["error"], $_FILES["file_xml"]["error"]);
	    	}

      		$xml = file_get_contents($_FILES["file_xml"]["tmp_name"]);
      		die($xml);


      		//pa_csv_load($_POST['table'], $xml);
		}
		catch (Exception $e) {
			$error_message = $e->getMessage();
			die("EXP: ".$error_message);
		}
		*/
	}

	//$icon_url= plugins_url().'/bsihotelbooking/images/plugin.ico';
	$icon_url = 'dashicons-admin-generic';

	// MENU: pa
	add_menu_page('Universa Importer', 'Universa Importer', 'manage_options', 'ui_menu', 'ui_menu', $icon_url, 0);
}

add_action('admin_menu', 'ui_dashboard');

function ui_menu()
{
    echo ui_theme('ui_menu');
}

add_action( 'init', 'stop_heartbeat', 1 );

function stop_heartbeat()
{
    wp_deregister_script('heartbeat');
}