<?php

class CSMWordpressImporter extends AbstractCMSImporter
{
    // Filters
    protected function _filter_images($text)
    {
        return str_replace("sites/", "wp-content/uploads/drupal/", $text);
    }

    protected function _import_taxonomies()
    {
        // create one category foreach content type
        $results = $this->xpath->query("//content_types/content_type");

        $length = $results->length;
        fg_log("Content types to import: ".$length);

        foreach ($results as $el) {
            $name = "Imported ".$el->getAttribute('name');
            $slug = 'import-'.$el->getAttribute('machine_name');

            $term = wp_insert_term(
        		$name,
        		'category',
        		array(
        		  'description'	=> '',
        		  'slug' 		=> $slug
        		)
        	);

        	if( is_wp_error( $term ) ) {
        	    echo $term->get_error_message();

        	    die();
        	}

        	$this->mapTaxonomyTerm($el->getAttribute('id'), $term['term_id']);
        }


        $results = $this->xpath->query("//taxonomy/terms/term");

        foreach ($results as $el) {
            $term = wp_insert_term(
        		$el->getAttribute('name'),
        		$el->getAttribute('taxonomy_name'),
        		array(
        		  'description'	=> $el->getAttribute('description'),
        		  'slug' 		=> $el->getAttribute('name')
        		)
        	);

        	if( is_wp_error( $term ) ) {
        	    echo $term->get_error_message();

        	    die();
        	}

        	$this->mapTaxonomyTerm($el->getAttribute('id'), $term['term_id']);
        }
    }

    protected function _import_post_fields($post_id, DOMNode $context)
    {
        $results = $this->xpath->query(".//field", $context);

        add_post_meta($post_id, 'original_url', $context->getAttribute('url'));
        add_post_meta($post_id, 'post_type', $context->getAttribute('content_type'));

        foreach ($results as $el) {
            $meta_key = $el->getAttribute("name");
            $meta_value = $el->getAttribute("value");

            add_post_meta($post_id, $meta_key, $meta_value);
        }
    }

    protected function _import_post_taxonomies($post_id, DOMNode $context)
    {
        $ids = array();

        $results = $this->xpath->query(".//term", $context);

        foreach ($results as $el) {
            $taxonomy = $el->getAttribute("taxonomy_name");
            $id = $el->getAttribute("id");

            $ids[$taxonomy][] = $this->getMappedTaxonomyTerm($id);
        }

        foreach ($ids as $taxonomy => $term_ids) {
            wp_set_object_terms($post_id, $term_ids, $taxonomy);
        }
    }

    protected function _import_contents()
    {
        $counter = 0;
        $started = time();

        $results = $this->xpath->query("//content");
        $length = $results->length;

        if ($length == 0) {
            fg_log("No contents to import");
            return ;
        }
        else {
            fg_log("Contents to import: ".$length);
        }

        ui_ajax_init_steps($length);

        foreach ($results as $el) {
            $title = $this->_get_element_value('./title', $el);
            $id = $el->getAttribute('id');
            $url = $el->getAttribute('url');
            $content_type = $el->getAttribute('content_type');
            $abstract = $this->_get_element_value('./abstract', $el);
            $body = $this->_get_element_value('./body', $el);

            // Filters
            $abstract = $this->_filter_images($abstract);
            $body = $this->_filter_images($body);

            ui_ajax_set_next_step(sprintf("Importing content %d/%d: #%d", ++$counter, $length, $id));

            $content = new CMSWordpressContent(
                $id,
                $title,
                $url,
                $body,
                $content_type
            );

            $content->setAbstract($abstract);
            $content->setDate($el->getAttribute('date'));
            $content->setLanguage($el->getAttribute('language'));

            $post_id = $content->create();

            if( is_wp_error( $post_id ) ) {
                echo $post_id->get_error_message();
                die();
            }

            // Migra tassonomie
            $this->_import_post_taxonomies($post_id, $el);

            // Migra fields
            $this->_import_post_fields($post_id, $el);
        }

        ui_ajax_set_next_step(sprintf("Completed in: %s.", ui_seconds_to_string(time() - $started)));
    }

    public function import($xml_file)
    {
        $counter = 0;

        if (ui_is_importer_executing()) {
            //return ;
        }

        ini_set('memory_limit', '1024M');
        set_time_limit (600);

        $this->_create_doc($xml_file);

        // Lock importer
        ui_lock_importer();

        // import taxonomies
        $this->_import_taxonomies();

        // import contents
        $this->_import_contents();

        // Unlock importer
        ui_unlock_importer();
    }
}
