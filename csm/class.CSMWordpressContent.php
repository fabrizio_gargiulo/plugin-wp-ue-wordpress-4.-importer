<?php

define ('CMS_WP_CONTENT_POST_TYPE', 'post');
define ('CMS_WP_CONTENT_POST_AUTHOR', 3);
define ('CMS_WP_CONTENT_COMMENT_STATUS', 'closed');

class CMSWordpressContent extends Content
{
    public function create()
    {
        // Default category assigned to current Content
        $post_category = array();

        $category_slug = 'import-'.$this->content_type;
        $category = get_category_by_slug($category_slug);

        if (!empty($category)) {
            $post_category[] = $category->term_id;
        }
        else {
            fg_log("CMSWordpressContent::create(), slug non presente: ".$category_slug);
        }

        $post = array(
            'post_content'   => $this->body,
            'post_name'      => sanitize_title_with_dashes($this->title),
            'post_title'     => $this->title,
            'post_status'    => 'publish',
            'post_type'      => CMS_WP_CONTENT_POST_TYPE,
        	'post_category'  => $post_category,
            'post_author'    => CMS_WP_CONTENT_POST_AUTHOR,
            'post_excerpt'   => $this->abstract,
            'comment_status' => CMS_WP_CONTENT_COMMENT_STATUS,
            'post_date'      => date("Y-m-d H:i:s", $this->date), // [ Y-m-d H:i:s ] // The time post was made.
  			'post_date_gmt'  => date("Y-m-d H:i:s", $this->date), // [ Y-m-d H:i:s ] // The time post was made, in GMT.
        );

        $post_id = wp_insert_post( $post, $wp_error );

        if (!$post_id) {
            fg_log("Impossibile importare il contenuto #%d dal titolo %s", $this->id, $this->title);
            return ;
        }

        // FONTE: http://wordpress.stackexchange.com/questions/20143/plugin-wpml-how-to-create-a-translation-of-a-post-using-the-wpml-api

        // Include WPML API
        include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );

        fg_log(sprintf("Provo a tradurre il contenuto %d nella lingua %s", $post_id, $this->language));
        //wpml_add_translatable_content('post_post', $post_id, $this->language);
        wpml_update_translatable_content('post_post', $post_id, $this->language);

        return $post_id;
    }
}