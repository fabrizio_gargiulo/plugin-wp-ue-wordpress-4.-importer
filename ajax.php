<?php

add_action( 'wp_ajax_ui_ajax_current_step', 'ui_ajax_current_step' );
add_action( 'wp_ajax_nopriv_ui_ajax_current_step', 'ui_ajax_current_step' );

function ui_ajax_current_step()
{
    $ui_step = get_option(UI_STEP, array(
        'current' => 0,
        'total' => 100,
        'step' => 0,
        'message' => get_option( 'ui_step', 0)
    ));

    fg_log("UI_STEP: ".serialize($ui_step));

	echo json_encode($ui_step);

	exit;
}

add_action( 'wp_ajax_ui_ajax_import', 'ui_ajax_import' );
add_action( 'wp_ajax_nopriv_ui_ajax_import', 'ui_ajax_import' );

function ui_ajax_import()
{
    require_once 'core/auto_loader.php';
    require_once 'csm/auto_loader.php';

    $importer = new CSMWordpressImporter();
    $xml_path = realpath(dirname(__FILE__)."/xml/universal-export.xml");
    $importer->import($xml_path);

	exit;
}