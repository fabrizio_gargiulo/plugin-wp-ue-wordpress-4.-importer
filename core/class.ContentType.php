<?php

/**
 *
 * In Drupal possono essere memorizzati, in Wordpress invece risiedono in un plugin/functions.php
 * @author fabriziogargiulo
 *
 */
class ContentType implements IExportable
{
    protected $machine_name, $name, $plural;

    public function __construct($machine_name, $name)
    {
        $this->machine_name = $machine_name;
        $this->name = $name;
        $this->plural = $name;
    }

    public function setPlural($plural)
    {
        $this->plural = $plural;
        return $this;
    }

    public function getMachineName()
    {
        return $this->machine_name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPlural()
    {
        return $this->plural;
    }

    public function toArray()
    {
        return array(
            'machine_name' => $this->machine_name,
            'name' => $this->name,
            'plural' => $this->plural
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement( "content_type" );

        $element->setAttribute('machine_name', $this->machine_name);
        $element->setAttribute('name', $this->name);
        $element->setAttribute('plural', $this->plural);

        return $element;
    }
}

function custom_register_post_type($type, $singular, $plural)
{
    register_post_type( $type, /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei men�
        array('labels' => array(
                'name' => $plural, /* Nome, al plurale, dell'etichetta del post type. */
                'singular_name' => $singular, /* Nome, al singolare, dell'etichetta del post type. */

                /*
                'all_items' => 'Tutti i '.ucfirst($plural),
                'add_new' => 'Aggiungi nuovo',
                'add_new_item' => 'Aggiungi nuovo '.$singular,
                'edit_item' => 'Modifica '.$singular,
                'new_item' => 'Nuovo '.$singular,
                'view_item' => 'Visualizza '.$singular,
                'search_items' => 'Cerca '.$singular,
                'not_found' =>  'Nessun '.$singular.' trovato',
                'not_found_in_trash' => 'Nessun '.$singular.' trovato nel cestino',
                'parent_item_colon' => ''
                */
            ),

            'description' => 'Raccolta di '.$plural.' del portale', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type � escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => 8, /* Definisce l'ordine in cui comparire nel men� di amministrazione a sinistra */
            //'menu_icon' => get_stylesheet_directory_uri() . '/img/custom-post-icon.png', /* Scegli l'icona da usare nel men� per il posty type */
            'rewrite'   => array( 'slug' => $plural, 'with_front' => false ), /* Puoi specificare uno slug per gli URL */
            'has_archive' => 'true', /* Definisci se abilitare la generazione di un archivio (equivalente di archive-distributori.php) */
            'capability_type' => 'post', /* Definisci se si comporter� come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */
}

function drupal_contenty_type_array()
{
    // Drupal
    $node_type = array(
        array(
    		'type' => 'article',
            'name' => 'Notizie',
            'base' => 'node_content',
            'module' => 'node',
            'description' => 'Use <em>articles</em> for time-sensitive content like news,
             press releases or blog posts.',
            'help' => '',
            'has_title' => '1',
            'title_label' => 'Title',
            'custom' => '1',
            'modified' => '1',
            'locked' => '0',
            'disabled' => '0',
            'orig_type' => 'article'
        ),
    );
}

