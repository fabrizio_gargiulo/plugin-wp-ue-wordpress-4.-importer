<?php

class Taxonomy implements IExportable
{
    // Mandatory
    protected $id, $name, $description, $terms;

    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->terms = array();
    }

    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addTerm($id, $name, $description)
    {
        return new CMSDrupalTaxonomyTerm($this, $id, $name, $description);
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        	'description' => $this->description,
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement("taxonomy");

        $element->setAttribute('id', $this->id);
        $element->setAttribute('name', $this->name);
        $element->setAttribute('description', $this->description);

        // Terms
        $terms = $doc->createElement('terms');

        foreach ($this->terms as $term) {
            $terms->appendChild($term->export($doc));
        }

        $element->appendChild($terms);

        return $element;
    }
}