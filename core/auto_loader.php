<?php
$this_dir = dirname(__FILE__);

function ue_autoload($directory, $type)
{
    if ($handle = opendir($directory)) {
    	/* Questa � la maniera corretta di eseguire un loop all'interno di una directory. */
        while (false !== ($file = readdir($handle))) {
            if (($file == '.') || ($file == '..')) {
                continue;
            }

            if (substr($file, 0, strlen($type)) == $type) {
                require_once $directory.'/'.$file;
            }
        }

        closedir($handle);
    }
}

//ue_autoload($this_dir, "interface");
//ue_autoload($this_dir, "class");

// @hack: sul sito di dario fa importanza l'ordine con il quale si caricano tali classi
require_once "interface.IExportable.php";
require_once "class.TaxonomyTerm.php";
require_once "class.User.php";
require_once "class.AbstractCMS.php";
require_once "class.AbstractCMSImporter.php";
require_once "class.AbstractCMSExporter.php";
require_once "class.Taxonomy.php";
require_once "class.Content.php";
require_once "class.ContentType.php";
require_once "class.DataBase.php";

?>