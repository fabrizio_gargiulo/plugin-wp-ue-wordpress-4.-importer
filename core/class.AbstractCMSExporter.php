<?php

// SOLO ESPORTAZIONE
abstract class AbstractCMSExporter extends AbstractCMS implements IExportable
{
    // Lettura
    abstract public function getUsers();
    abstract public function getTaxonomies();
    abstract public function getContentTypes();
    abstract public function getContents();

    public function toArray()
    {
        $export = array();

        // Infos
        $export['infos'] = array(
            'start' => date('Y-m-d H:i:s'),
        	'end' => date('Y-m-d H:i:s'),
        );

        // Utenti
        $export['users'] = $this->exportUsers();

        // Content types
        $export['content_types'] = $this->exportContentTypes();

        // Articoli
        $export['contents'] = $this->exportContents();

        // Infos
        $export['infos']['end'] = date('Y-m-d H:i:s');

        return $export;
    }

    public function export(DOMDocument $doc = null)
    {
        $started = time();
        $xml_path = realpath(dirname(__FILE__)."/../xml/")."/universal-export.xml";

        // "Create" the document.
        $doc = new DOMDocument( "1.0", "UTF-8" );
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;


        $export = $doc->createElement("export");
        $users = $doc->createElement("users");
        $taxonomies = $doc->createElement("taxonomies");
        $content_types = $doc->createElement("content_types");
        $contents = $doc->createElement("contents");

        // Creo XML users
        foreach ($this->getUsers() as $user) {
            $users->appendChild(
                $user->export($doc)
            );
        }

        // Creo XML taxonomies
        foreach ($this->getTaxonomies() as $taxonomy) {
            $taxonomies->appendChild(
                $taxonomy->export($doc)
            );
        }

        // Creo XML content types
        foreach ($this->getContentTypes() as $content_type) {
            $content_types->appendChild(
                $content_type->export($doc)
            );
        }

        // Creo XML contents
        foreach ($this->getContents() as $content) {
            $contents->appendChild(
                $content->export($doc)
            );
        }

        $export->appendChild($users);
        $export->appendChild($taxonomies);
        $export->appendChild($content_types);
        $export->appendChild($contents);
        $doc->appendChild($export);

        //header('Content-disposition: attachment; filename="universal-export.xml"');
        //header('Content-type: "text/xml"; charset="utf8"');

        // Completition
        $export->setAttribute('elapsed', time() - $started);

        try {
            $doc->save($xml_path);
        }
        catch (Exception $e) {
            die("Error:".$e->getMessage());
        }
        //echo $doc->saveXML();
    }
}