<?php

class TaxonomyTerm implements IExportable
{
    // Mandatory
    protected $taxonomy, $id, $name, $description;

    public function __construct(Taxonomy $taxonomy, $id, $name, $description)
    {
        $this->taxonomy = $taxonomy;
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        	'description' => $this->description,
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement("term");

        $element->setAttribute('taxonomy_id', $this->taxonomy->getID());
        $element->setAttribute('taxonomy_name', $this->taxonomy->getName());
        $element->setAttribute('id', $this->id);
        $element->setAttribute('name', $this->name);
        $element->setAttribute('description', $this->description);

        return $element;
    }
}